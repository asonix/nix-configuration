{ config, pkgs, nixpkgs-unstable, obs-scene-switcher, obs-streamfx, ... }:

let
  unstable = import nixpkgs-unstable {
    system = "x86_64-linux";
  };
in
{
  imports = [ ./zsh.nix ./ssh.nix ];

  home.username = "asonix";
  home.homeDirectory = "/home/asonix";

  nixpkgs.overlays = [
    (self: super: {
      obs-studio-plugins = super.obs-studio-plugins // {
        obs-scene-switcher = obs-scene-switcher.obs-scene-switcher;
        obs-streamfx = obs-streamfx.obs-streamfx;
      };
      neovim = unstable.neovim;
      nextcloud-client = unstable.nextcloud-client;
    })
  ];

  home.packages = with pkgs; [
    bat
    bottom
    dig
    exa
    htop
    neofetch
    starship
    topgrade

    carla
    cura
    freecad
    gnome.dconf-editor
    helvum
    nextcloud-client
    patchage
    pavucontrol
    tdesktop
    vlc

    neovim
  ];

  home.stateVersion = "22.05";

  programs.home-manager.enable = true;
  programs.git = {
    enable = true;
    userName = "asonix";
    userEmail = "asonix@asonix.dog";
  };

  programs.bat.enable = true;

  programs.obs-studio = {
    enable = true;
    plugins = with pkgs.obs-studio-plugins; [
      input-overlay
      obs-backgroundremoval
      obs-gstreamer
      obs-move-transition
      obs-multi-rtmp
      obs-nvfbc
      obs-pipewire-audio-capture
      obs-scene-switcher
      obs-source-record
      obs-streamfx
      obs-vkcapture
      wlrobs
    ];
  };
}
