{ config, pkgs, ... }:

let
  server = ({ hostname, user ? "asonix", port ? 22, proxyJump ? true }: {
    hostname = hostname;
    user = user;
    identitiesOnly = true;
    identityFile = "/home/asonix/.ssh/kube-rsa";
    port = port;
    proxyJump = if proxyJump then "router" else null;
  });
in
{
  programs.ssh = {
    enable = true;

    matchBlocks = {
      "github.com" = {
        hostname = "github.com";
        user = "git";
        identitiesOnly = true;
        identityFile = "/home/asonix/.ssh/github";
        port = 22;
      };
      "gitlab.com" = {
        hostname = "gitlab.com";
        user = "git";
        identitiesOnly = true;
        identityFile = "/home/asonix/.ssh/gitlab";
        port = 22;
      };
      "git.asonix.dog" = {
        hostname = "git.asonix.dog";
        user = "git";
        identitiesOnly = true;
        identityFile = "/home/asonix/.ssh/gitea-key";
        port = 22;
      };
      "router" = server {
        hostname = "ssh.asonix.dog";
        port = 3128;
        proxyJump = false;
      };
      "mc1" = server { hostname = "192.168.20.100"; };
      "build2" = server { hostname = "192.168.20.99"; };
      "bluestar" = server { hostname = "192.168.20.36"; };
      "nextcloud" = server { hostname = "192.168.20.21"; };
      "nextcloud2" = server { hostname = "192.168.20.28"; };
      "lionheart" = server { hostname = "192.168.5.6"; };
      "redtail" = server { hostname = "192.168.20.23"; };
      "redtail2" = server { hostname = "192.168.20.24"; };
      "whitestorm" = server { hostname = "192.168.20.11"; };
      "whitestorm2" = server { hostname = "192.168.20.26"; };
      "gluster2" = server { hostname = "192.168.20.19"; user = "kube"; };
      "gluster3" = server { hostname = "192.168.20.25"; };
      "gluster4" = server { hostname = "192.168.20.33"; };
      "k3s1" = server { hostname = "192.168.20.120"; };
      "k3s2" = server { hostname = "192.168.20.121"; };
      "k3s3" = server { hostname = "192.168.20.122"; };
      "k3s4" = server { hostname = "192.168.20.123"; };
      "k3s5" = server { hostname = "192.168.20.124"; };
      "k3s6" = server { hostname = "192.168.20.125"; };
      "k3s-rock1" = server { hostname = "192.168.20.20"; };
      "k3s-rock2" = server { hostname = "192.168.20.111"; };
      "k3s-rock3" = server { hostname = "192.168.20.112"; };
      "k3s-rock4" = server { hostname = "192.168.20.113"; };
      "k3s-rock5" = server { hostname = "192.168.20.114"; };
      "k3s-quartza1" = server { hostname = "192.168.20.160"; };
      "k3s-rockpro1" = server { hostname = "192.168.20.180"; };
    };
  };
}
