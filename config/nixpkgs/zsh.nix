{ config, pkgs, ... }:

{
  programs.zsh = {
    enable = true;
    dotDir = ".config/zsh";
    enableAutosuggestions = true;
    shellAliases = {
      cat = "bat";
      l = "exa -l";
      la = "exa -la";
      ls = "exa";
    };

    initExtra = ''
      bindkey -v

      if [ "$IN_NIX_SHELL" = "impure" ]; then
        alias b='cargo build'
        alias c='cargo check'
        alias r='cargo run'
        alias t='cargo check'
        alias v=lvim
        alias vi=lvim
        alias vim=lvim
      else
        alias v=nvim
        alias vi=nvim
        alias vim=nvim
	alias dev="nix-shell $HOME/Development/default.nix"
      fi

      eval "$(starship init zsh)"
    '';

    plugins = with pkgs; [
      {
        name = "base16-shell";
        src = fetchFromGitHub {
          owner = "chriskempson";
          repo = "base16-shell";
          rev = "ae84047d378700bfdbabf0886c1fb5bb1033620f";
          sha256 = "0qy+huAbPypEMkMumDtzcJdQQx5MVgsvgYu4Em/FGpQ=";
        };
        file = "base16-shell.plugin.zsh";
      }
      {
        name = "zsh-completions";
        src = fetchFromGitHub {
          owner = "zsh-users";
          repo = "zsh-completions";
          rev = "0.34.0";
          sha256 = "qSobM4PRXjfsvoXY6ENqJGI9NEAaFFzlij6MPeTfT0o=";
        };
        file = "zsh-completions.plugin.zsh";
      }
      {
        name = "zsh-nix-shell";
        src = fetchFromGitHub {
          owner = "chisui";
          repo = "zsh-nix-shell";
          rev = "v0.5.0";
          sha256 = "IT3wpfw8zhiNQsrw59lbSWYh0NQ1CUdUtFzRzHlURH0=";
        };
        file = "nix-shell.plugin.zsh";
      }
      {
        name = "zsh-syntax-highlighting";
        src = fetchFromGitHub {
          owner = "zsh-users";
          repo = "zsh-syntax-highlighting";
          rev = "0.7.1";
          sha256 = "gOG0NLlaJfotJfs+SUhGgLTNOnGLjoqnUp54V9aFJg8=";
        };
        file = "zsh-syntax-highlighting.zsh";
      }
      {
        name = "zsh-z";
        src = fetchFromGitHub {
          owner = "agkozak";
          repo = "zsh-z";
          rev = "aaafebcd97424c570ee247e2aeb3da30444299cd";
          sha256 = "9Wr4uZLk2CvINJilg4o72x0NEAl043lP30D3YnHk+ZA=";
        };
        file = "zsh-z.plugin.zsh";
      }
    ];
  };
}
