{ pkgs ? import <nixpkgs> { } }:

pkgs.mkShell {
  buildInputs = with pkgs; [
    ack
    cargo-outdated
    code-minimap
    elmPackages.elm
    elmPackages.create-elm-app
    elmPackages.elm-format
    elmPackages.elm-language-server
    elmPackages.elm-optimize-level-2
    elmPackages.elm-review
    elmPackages.elm-test
    elmPackages.nodejs
    exiftool
    fd
    ffmpeg-full
    file
    icu
    imagemagick
    jq
    libidn
    librsvg
    libwebp
    openssl
    openssl_3
    pkg-config
    postgresql
    protobuf
    python39Full
    python39Packages.aiofiles
    python39Packages.aiohttp
    python39Packages.pynvim
    ripgrep
    ruby_3_0
    rustup
    taplo-lsp
    taplo-cli
    tokei
    tree-sitter
    yarn
    zig
    zlib
    zlib-ng
  ];

  shellHook = ''

    export PATH=$HOME/.local/bin:$PATH:$HOME/.cargo/bin

    eval "$(starship init bash)"
  '';

  RUST_BACKTRACE = 1;
}
