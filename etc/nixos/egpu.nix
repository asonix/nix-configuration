{ config, pkgs, lib, ... }:

let
  keyValues = (key: values: lib.strings.concatStringsSep " " [
    key
    (lib.strings.concatMapStringsSep " " (value: lib.strings.concatStrings [ "\"" value "\"" ]) values)
  ]);

  keyValue = (key: value: keyValues key [ value ]);

  section = (name: strings: lib.strings.concatStrings [
    (keyValue "Section" name)
    (lib.strings.concatStringsSep "\n  " strings)
    "EndSection"
  ]);

  nvidia = ({ side, busID }: {
    system.nixos.tags = [ "eGPU" side ];

    # boot.kernelPackages = lib.mkForce pkgs.linuxPackages;

    # Set X11 to use Nvidia drivers
    services.xserver.videoDrivers = [ "nvidia" ];

    hardware.opengl.enable = true;
    hardware.nvidia.package = config.boot.kernelPackages.nvidiaPackages.stable;
    services.xserver.config = pkgs.lib.mkOverride 0 (lib.strings.concatStringsSep "\n\n" [
      (section "Module" [ (keyValue "Load" "modesetting") ])
      (section "Device" [
        (keyValue "Identifier" "Device0")
        (keyValue "Driver" "nvidia")
        (keyValue "BusID" busID)
        (keyValue "Option" "AllowEmptyInitialConfiguration")
        (keyValues "Option" [ "AllowExternalGpus" "True" ])
      ])
    ]);
  });
in {
  # eGPU specialisation config 
  specialisation = {
    eGPUright.configuration = nvidia { side = "right"; busID = "4:0:0"; };

    eGPUleft.configuration = nvidia { side = "left"; busID = "2:0:0"; };
  };
}
