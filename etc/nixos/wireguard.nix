{ config, pkgs, ... }:

{
  networking.firewall = {
    allowedUDPPorts = [ 51820 ];
  };

  networking.wg-quick.interfaces = {
    wg0 = {
      address = [ "192.168.5.10/24" ];
      dns = [ "192.168.5.1" ];
      privateKeyFile = "/etc/wireguard/privatekey";
      peers = [
        {
          publicKey = "lQYGzNIxgUrDmU32rlnmnc72dK7TSH7hxts3tMtE+VQ=";
          allowedIPs = [ "192.168.5.0/24" "192.168.6.0/24" "192.168.20.0/24" ];
          endpoint = "wg.asonix.dog:51820";
          persistentKeepalive = 25;
        }
      ];
    };
  };
}
