# Edit this configuration file to define what should be installed on
# your system.  Help is available in the configuration.nix(5) man page
# and in the NixOS manual (accessible by running ‘nixos-help’).

{ config, pkgs, ... }:

{
  imports =
    [
      # Include the results of the hardware scan.
      ./hardware-configuration.nix
      ./wireguard.nix
    ];

  # Nix cache
  # nix.extraOptions = ''
  # secret-key-files = /etc/nix/cache-priv-key.pem
  # '';
  # Bootloader.
  boot.loader.systemd-boot.enable = true;
  boot.loader.efi.canTouchEfiVariables = true;
  boot.loader.efi.efiSysMountPoint = "/boot/efi";

  # Setup keyfile
  boot.initrd.secrets = {
    "/crypto_keyfile.bin" = null;
    "/keyfile" = null;
  };

  # Enable swap on luks
  boot.initrd.luks.devices."luks-28ea4268-e74a-4ed7-9484-b113c676124e".device = "/dev/disk/by-uuid/28ea4268-e74a-4ed7-9484-b113c676124e";
  boot.initrd.luks.devices."luks-28ea4268-e74a-4ed7-9484-b113c676124e".keyFile = "/crypto_keyfile.bin";

  boot.initrd.luks.devices."cryptdrive4".device = "/dev/disk/by-uuid/d2119824-fe98-449b-9d1b-2ab552568493";
  boot.initrd.luks.devices."cryptdrive4".keyFile = "/keyfile";

  boot.initrd.luks.devices."cryptdrive3".device = "/dev/disk/by-uuid/99dd440d-c6ed-4149-85a1-e8f22a6f2535";
  boot.initrd.luks.devices."cryptdrive3".keyFile = "/keyfile";

  boot.initrd.luks.devices."cryptdrive2".device = "/dev/disk/by-uuid/fd0c26d4-db05-4218-826c-51a87dd39eb5";
  boot.initrd.luks.devices."cryptdrive2".keyFile = "/keyfile";

  boot.initrd.luks.devices."cryptdrive1".device = "/dev/disk/by-uuid/20515efa-5380-4116-946e-7fe527ed3b92";
  boot.initrd.luks.devices."cryptdrive1".keyFile = "/keyfile";

  boot.kernelPackages = pkgs.linuxPackages_latest;

  boot.binfmt.emulatedSystems = [ "aarch64-linux" ];

  fileSystems = {
    "/home/asonix/Development".options = [ "compress=zstd" ];
    "/home/asonix/Diskimages".options = [ "compress=zstd" ];
    "/home/asonix/Documents".options = [ "compress=zstd" ];
    "/home/asonix/Downloads".options = [ "compress=zstd" ];
    "/home/asonix/Games".options = [ "compress=zstd" ];
    "/home/asonix/Games2".options = [ "compress=zstd" ];
    "/home/asonix/Music".options = [ "compress=zstd" ];
    "/home/asonix/Pictures".options = [ "compress=zstd" ];
    "/home/asonix/Videos".options = [ "compress=zstd" ];
  };

  networking.hostName = "firestar"; # Define your hostname.
  # networking.wireless.enable = true;  # Enables wireless support via wpa_supplicant.

  # Configure network proxy if necessary
  # networking.proxy.default = "http://user:password@proxy:port/";
  # networking.proxy.noProxy = "127.0.0.1,localhost,internal.domain";

  # Enable networking
  networking.networkmanager.enable = true;

  # This makes Spyro Reignighted Trilogy not crash
  networking.extraHosts =
    ''
      0.0.0.0 datarouter.ol.epicgames.com
      0.0.0.0 datarouter-weighted.ol.epicgames.com
    '';

  # Set your time zone.
  time.timeZone = "America/Chicago";

  # Select internationalisation properties.
  i18n.defaultLocale = "en_US.utf8";

  # Enable the X11 windowing system.
  services.xserver.enable = true;

  # Enable the Pantheon Desktop Environment.
  services.xserver.displayManager.lightdm.enable = true;
  services.xserver.desktopManager.pantheon.enable = true;

  # Configure keymap in X11
  services.xserver = {
    layout = "us";
    xkbVariant = "";
  };

  # Enable CUPS to print documents.
  services.printing.enable = true;
  services.avahi.enable = true;
  services.avahi.openFirewall = true;
  # services.ipp-usb.enable = true;

  # Enable sound with pipewire.
  sound.enable = true;
  hardware.pulseaudio.enable = false;
  security.rtkit.enable = true;
  services.pipewire = {
    enable = true;
    alsa.enable = true;
    alsa.support32Bit = true;
    pulse.enable = true;
    # If you want to use JACK applications, uncomment this
    jack.enable = true;

    # use the example session manager (no others are packaged yet so this is enabled by default,
    # no need to redefine it in your config for now)
    #media-session.enable = true;
  };

  # Enable touchpad support (enabled default in most desktopManager).
  # services.xserver.libinput.enable = true;

  services.flatpak.enable = true;

  xdg.portal.extraPortals = [ pkgs.xdg-desktop-portal-gtk ];

  # Define a user account. Don't forget to set a password with ‘passwd’.
  users.users.asonix = {
    isNormalUser = true;
    description = "Tavi";
    shell = pkgs.zsh;
    extraGroups = [ "networkmanager" "wheel" ];
    packages = with pkgs; [
      firefox
    ];
  };

  users.defaultUserShell = pkgs.zsh;

  # Allow unfree packages
  nixpkgs.config.allowUnfree = true;

  # List packages installed in system profile. To search, run:
  # $ nix search wget
  environment.systemPackages = with pkgs; [
    docker-compose
    git
    vim
    zsh

    protonup
    protontricks
    wine-staging
    winetricks

    usbutils
    pciutils
    bolt
    thunderbolt
    wireguard-tools
  ];

  systemd.packages = with pkgs; [
    bolt
  ];

  fonts = {
    enableDefaultFonts = true;

    fonts = with pkgs; [
      fantasque-sans-mono
      (nerdfonts.override { fonts = [ "FantasqueSansMono" ]; })
    ];

    fontconfig.defaultFonts.monospace = [ "Fantasque Sans Mono" "Roboto Mono" ];

    fontDir.enable = true;
  };

  programs.zsh.enable = true;
  programs.vim.defaultEditor = true;

  programs.steam = {
    enable = true;
    remotePlay.openFirewall = true;
    dedicatedServer.openFirewall = true;
  };

  # Some programs need SUID wrappers, can be configured further or are
  # started in user sessions.
  # programs.mtr.enable = true;
  # programs.gnupg.agent = {
  #   enable = true;
  #   enableSSHSupport = true;
  # };

  # List services that you want to enable:

  # Enable the OpenSSH daemon.
  # services.openssh.enable = true;

  services.fwupd.enable = true;
  services.fwupd.enableTestRemote = true;
  services.fwupd.extraRemotes = [
    "lvfs-testing"
  ];

  virtualisation.docker.enable = true;

  # Open ports in the firewall.
  # networking.firewall.allowedTCPPorts = [ ... ];
  # networking.firewall.allowedUDPPorts = [ ... ];
  # Or disable the firewall altogether.
  # networking.firewall.enable = false;

  nix.settings.experimental-features = [ "nix-command" "flakes" ];

  # This value determines the NixOS release from which the default
  # settings for stateful data, like file locations and database versions
  # on your system were taken. It‘s perfectly fine and recommended to leave
  # this value at the release version of the first install of this system.
  # Before changing this value read the documentation for this option
  # (e.g. man configuration.nix or on https://nixos.org/nixos/options.html).
  system.stateVersion = "22.05"; # Did you read the comment?

}
